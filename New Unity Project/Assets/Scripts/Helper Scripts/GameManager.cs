﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private ScoreManagerScript theScoreManager;
    public GameOverMenuScript theGameOverScreen;

    void Start()
    {
        theScoreManager = FindObjectOfType<ScoreManagerScript>();
    }

    void Awake()
    { 
        if (instance == null)
            instance = this;
    }

    public void RestartGame()
    {
        theScoreManager.scoreGoesUp = false;
        theGameOverScreen.gameObject.SetActive(true);
        //Invoke("RestartAfterTime", 2f);
    }

    void RestartAfterTime()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Gameplay");
    }
}
