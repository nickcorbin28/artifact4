﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewMainMenu : MonoBehaviour
{
    public GameObject mainMenuUI;

    public void PlayGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Gameplay");
    }

    public void ResetHighScore()
    {
        PlayerPrefs.DeleteAll();
    }

    public void QuitGameFromMenu()
    {
        if (UnityEditor.EditorApplication.isPlaying == true)
        {

            UnityEditor.EditorApplication.isPlaying = false;

        }
        else
        {

            Application.Quit();

        }
    }
}
