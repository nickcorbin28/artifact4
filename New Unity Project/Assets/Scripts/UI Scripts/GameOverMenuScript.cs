﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverMenuScript : MonoBehaviour
{
    public string mainMenuLevel;

    public void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Gameplay");
    }

    public void QuitGame()
    {
        if (UnityEditor.EditorApplication.isPlaying == true)
        {

            UnityEditor.EditorApplication.isPlaying = false;

        }
        else
        {

            Application.Quit();

        }
    }
}
