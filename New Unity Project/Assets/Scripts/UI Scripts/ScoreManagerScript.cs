﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManagerScript : MonoBehaviour
{
    public Text scoreText;

    public Text highScoreText;

    public float scoreCount;

    public float highScoreCount;

    public float pointsPerSecond;

    public bool scoreGoesUp;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScoreCount = PlayerPrefs.GetFloat("HighScore", 0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (scoreGoesUp)
        {
            scoreCount += pointsPerSecond * Time.deltaTime;
        }

        if(scoreCount > highScoreCount)
        {
            highScoreCount = scoreCount;
            PlayerPrefs.SetFloat("HighScore", highScoreCount);
        }

        scoreText.text = "Score: " + Mathf.Round (scoreCount);

        highScoreText.text = "High Score: " + Mathf.Round (highScoreCount);
    }
}